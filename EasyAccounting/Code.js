function onOpen() {
  var ui = SpreadsheetApp.getUi();
  ui.createAddonMenu()
    .addItem('Setup', 'setUp')
    .addItem('Create Example', 'createExample')
    .addItem('Billing', 'billing')
    .addItem('About', 'showDialog')
    .addToUi();
}

function showDialog() {
  var html = HtmlService.createHtmlOutputFromFile('EasyAccounting/AboutDialog')
  SpreadsheetApp.getUi() // Or DocumentApp or SlidesApp or FormApp.
      .showModalDialog(html, 'About');
}

function billing() {
  var sumSheetName = 'Persons Accounting Areas';
  var billingSheetName = 'Billing';
  
  var sumSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sumSheetName);
  var billingSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(billingSheetName);

  deleteAutoRows(billingSheet);
  setUpCompensationPayments();
  processDebtors(billingSheet, sumSheet);
}

function processDebtors(billingSheet, sumSheet) {
  var balance = 3;
  var negligibleAmount = -0.10;
  var data = getData(sumSheet);  
  var dataCopy = [...data];
  
  // iterate over negative balance
  for (i = 0; i < dataCopy.length; i++) {
    // dont create transactions worth less then 0,1€
    if (dataCopy[i][balance] < negligibleAmount) {            
      processCreditors(billingSheet, dataCopy, dataCopy[i]);
      //i = 0;
    }
  }
}

function processCreditors(sheet, data, debtor) {
  var u = 0;
  var negligibleAmount = -0.10;
  var balance = 3;
  
  newData = data.slice();
  newData.reverse();
  
  while (debtor[balance] < negligibleAmount) {  
    if (newData.length == u) {
      break;
    }
    matchCreditorWithDebtor(sheet, debtor, newData[u]);
    u++;
  }
}

function matchCreditorWithDebtor(sheet, debtor, creditor) {
  var nameColumn = 0;
  var accountingAreaColumn = 1;
  var balance = 3;
  var compensationPayments = [];

  if (creditor[balance] > 0 && creditor[accountingAreaColumn] == debtor[accountingAreaColumn]) {
    if (creditor[balance] + debtor[balance] >= 0) {
      printSettlement(sheet, debtor[nameColumn], creditor[nameColumn], debtor[balance], debtor[accountingAreaColumn]);   
      compensationPayments = [
        debtor[nameColumn],
        creditor[nameColumn],
        -debtor[balance]
      ];     
      creditor[balance] += debtor[balance];
      debtor[balance] = 0;
    }
    else {
      printSettlement(sheet, debtor[nameColumn], creditor[nameColumn], -creditor[balance], debtor[accountingAreaColumn]); 
      compensationPayments = [
        debtor[nameColumn],
        creditor[nameColumn],
        creditor[balance]
      ];         
      debtor[balance] += creditor[balance];
      creditor[balance] = 0;
    }
    printCompensationPayment(compensationPayments);
  }
}

function getData(sheet) {
  var data = sheet.getDataRange().getValues();
  data.sort(compareColumn);
  return(data);
}

function printSettlement(sheet, name, reason, amount, accountingArea) {
  var date = formatDate();
  createRange(sheet.getName(), sheet.getLastRow() + 1, 1, 2, 6).setValues(
    [
      [name, 'pays ' + reason, -amount, date, accountingArea, 'x'],
      [reason, 'gets payed by ' + name, amount, date, accountingArea, 'x']
    ]
  );
}

function formatDate() {  
  var date = new Date();
  var dateText = date.toLocaleDadata_copCing();
  return(dateText);  
}

function printCompensationPayment(compensationPayment) {
  var debtor = 0;
  var creditor = 1;
  var amount = 2;
  var sumSheetName = 'Compensation Payments';
  var sumSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sumSheetName);
  var data = sumSheet.getDataRange().getValues();
  var printed = false;
  for (i = 1; i < data.length; i++) {
    if (compensationPayment[debtor] == data[i][debtor] && compensationPayment[creditor] == data[i][creditor]) {
      createRange(sumSheetName, i + 1, 1, 1, 3).setValues([
        [
          compensationPayment[debtor],
          compensationPayment[creditor],
          data[i][amount] + compensationPayment[amount]
        ]
      ]);
      printed = true;
    }
  }
  if (!printed) {
    createRange(sumSheetName, sumSheet.getLastRow() + 1, 1, 1, 3).setValues([
      compensationPayment
    ]);
  }
}

function deleteAutoRows(sheet) {  
  var autoRowsIndexes = getAutoRows(sheet);

  if (autoRowsIndexes.length > 0) {
    createRange(sheet.getName(), autoRowsIndexes[0] + 1, 1, sheet.getLastRow(), 6).clearContent();
  }
}

function getAutoRows(sheet) {
  var automaticCreatedRowNo = 5;
  var autoRowsIndexes = [];
  var data = sheet.getDataRange().getValues();

  for (i = 0; i < data.length; i++) {
    if (data[i][automaticCreatedRowNo] == 'x') {
      autoRowsIndexes.push(i)
    }
  }
  return autoRowsIndexes;
}

function compareColumn(a, b) {
  if (a[3] === b[3]) {
      return 0;
  }
  else {
      return (a[3] < b[3]) ? -1 : 1;
  }
}
