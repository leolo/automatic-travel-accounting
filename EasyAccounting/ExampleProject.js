function createExample() {
    setUp();
    createExamplePersons();
    createExampleAccountingAreas();
    createExamplePersonsAccountingArea();
    createExampleAccounting();
}

function createExamplePersons() {
  var sheetName = 'Persons';
  var exampleRows = [
    ['Ida'],
    ['Caspar'],
    ['Leo']
  ];
  createRange(sheetName, 2, 1, 3, 1).setValues(exampleRows);
}

function createExampleAccountingAreas() {
  var sheetName = 'Accounting Areas';
  var exampleRows = [
    ['General'],
    ['Ski'],
    ['Beer']
  ];
  createRange(sheetName, 2, 1, 3, 1).setValues(exampleRows);
}

function createExamplePersonsAccountingArea() {
  var sheetName = 'Persons Accounting Areas';
  var exampleRows = [
    ['Ida', 'General'],
    ['Ida', 'Beer'],
    ['Caspar', 'General'],
    ['Caspar', 'Beer'],
    ['Caspar', 'Ski'],
    ['Leo', 'General'],
    ['Leo', 'Ski']
  ];
  createRange(sheetName, 2, 1, 7, 2).setValues(exampleRows);
}

function createExampleAccounting() {
  var sheetName = 'Billing';
  var exampleRows = [
    ['Ida', 'groceries', 100.55, formatDate(), 'General', ''],
    ['Ida', 'beer!', 22.17, formatDate(), 'Beer', ''],
    ['Ida', 'sweeties', 12.19, formatDate(), 'General', ''],
    ['Caspar', 'rolls', 4.68, formatDate(), 'General', ''],
    ['Caspar', 'ski board', 12.34, formatDate(), 'Ski', ''],
    ['Caspar', 'ski rent', 140.99, formatDate(), 'Ski', ''],
    ['Leo', 'ski ticket', 190, formatDate(), 'Ski', ''],
    ['Leo', 'shopping Salzburg', 45.47, formatDate(), 'General', '']
  ];
  createRange(sheetName, 2, 1, 8, 6).setValues(exampleRows);
}