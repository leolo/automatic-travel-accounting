function setUp() {
  setUpSheets();
  setUpPersons(14);
  setUpAccountingArea(9);
  setUpPersonsAccountingArea(24);
  setUpAccounting(99);
}
  
function setUpSheets() {
  var notFound = -1;
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  var ssSheetNames = [];
  var sheetNames = [
      'Billing',
      'Persons',
      'Accounting Areas',
      'Persons Accounting Areas'
  ];

  sheets.forEach(function(sheet) {
      var sheet_name = sheet.getName()
      ssSheetNames.push(sheet_name);
  });
                          
  sheetNames.forEach(function(sheet) {
      if (ssSheetNames.indexOf(sheet) === notFound) {
      ss.insertSheet(sheet);
      }
  });
}

function setUpPersons(setup_rows) {
  var sheetName = 'Persons';
  var rowNames = [['Name', 'Expenses']];
  var spendPerPerson = "=SUM(IFERROR(FILTER(Billing!$C$2:C; A2=Billing!$A$2:$A);))";
  createRange(sheetName, 1, 1, 1, 2).setValues(rowNames);
  createRange(sheetName, 2, 2, setup_rows, 1).setFormula(spendPerPerson);
  createRange(sheetName, 2, 2, setup_rows, 1).setNumberFormat("0.00 €");
}

function setUpAccountingArea(setup_rows) {
  var sheetName = 'Accounting Areas';
  var rowNames = [['Name', 'Expenses']];
  spendPerAccountingArea = "=Sum(IFERROR(Filter(Billing!$C$2:$C;Billing!$E$2:$E =A2);))";
  createRange(sheetName, 1, 1, 1, 2).setValues(rowNames);
  createRange(sheetName, 2, 2, setup_rows, 1).setFormula(spendPerAccountingArea);
  createRange(sheetName, 2, 2, setup_rows, 1).setNumberFormat("0.00 €");
}

function setUpPersonsAccountingArea(setup_rows) {
  var sheetName = 'Persons Accounting Areas';
  var rowNames = [['Person Name', 'Accounting Area', 'Expanses', 'Balance', 'Distribution Key (in %)']];
  var spendFormula = "=Sum(IFERROR(Filter(Billing!$C$2:$C;Billing!$A$2:$A = A2;Billing!$E$2:$E =B2);))";
  var balanceFormula = "=C2 - ROUND(Filter('Accounting Areas'!$B$2:$B;'Accounting Areas'!$A$2:$A = B2) * IF(NOT(ISBLANK(E2)); E2 / 100; 1) / IF(ISBLANK(E2); COUNTA(FILTER($A$2:A; $B$2:B = B2)); 1); 2)";
  createRange(sheetName, 1, 1, 1, 5).setValues(rowNames);
  createRange(sheetName, 2, 3, setup_rows, 1).setFormula(spendFormula);
  createRange(sheetName, 2, 4, setup_rows, 1).setFormula(balanceFormula);
  createRange(sheetName, 2, 3, setup_rows, 2).setNumberFormat("0.00 €");
  var range = createRange(sheetName, 2, 4, setup_rows, 1);
  var balanced = SpreadsheetApp.newConditionalFormatRule()
      .whenNumberGreaterThanOrEqualTo(-0.01)
      .setBackground("#b7e1cd")
      .setRanges([range])
      .build()
  var notBalanced = SpreadsheetApp.newConditionalFormatRule()
      .whenNumberLessThan(-0.01)
      .setBackground("#f4cccc")
      .setRanges([range])
      .build()
  var personAccountingAreaSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheetName);
  var rules = personAccountingAreaSheet.getConditionalFormatRules();
  rules = [];
  rules.push(balanced);
  rules.push(notBalanced);
  personAccountingAreaSheet.setConditionalFormatRules(rules);
  createDropdown(
    createRange(sheetName, 2, 1, setup_rows, 1), 
    createRange('Persons', 2, 1, setup_rows, 1)
  );
  createDropdown(
    createRange(sheetName, 2, 2, setup_rows, 1),
    createRange('Accounting Areas', 2, 1, setup_rows, 1)
  );
}

function setUpAccounting(setup_rows) {
  var sheetName = 'Billing';
  var rowNames = [['Name', 'Subject', 'Amount', 'Date', 'Accounting Area', 'Final Billing']];
  createRange(sheetName, 1, 1, 1, 6).setValues(rowNames);
  createRange(sheetName, 2, 3, setup_rows, 1).setNumberFormat("0.00 €");
  createDropdown(
    createRange(sheetName, 2, 1, setup_rows, 1),
    createRange('Persons', 2, 1, setup_rows, 1)
  );
  createDropdown(
    createRange(sheetName, 2, 5, setup_rows, 1),
    createRange('Accounting Areas', 2, 1, setup_rows, 1)
  );
}

function setUpCompensationPayments() {
  var notFound = -1;
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  var sheetName = 'Compensation Payments';
  var rowNames = [['Debtor', 'Creditor', 'Amount']];
  var ssSheetNames = [];
  sheets.forEach(function(sheet) {
    var sheet_name = sheet.getName()
    ssSheetNames.push(sheet_name);
  })
  if (ssSheetNames.indexOf(sheetName) === notFound) {
    ss.insertSheet(sheetName);
    createRange(sheetName, 1, 1, 1, 3).setValues(rowNames);
  }
  createRange(sheetName, 2, 3, 50, 1).setNumberFormat("0.00 €");
  var noOfRows = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheetName).getLastRow() -1;
  if (noOfRows > 0) {
    createRange(sheetName, 2, 1, noOfRows, 3).clearContent();
  }
}

function createRange(sheet_name, row, column, noOfRows, noOfColumns) {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheet_name);
  var range = sheet.getRange(row, column, noOfRows, noOfColumns)
  return range
}

function createDropdown(range, dropdown_values) {
  var dropdown = SpreadsheetApp.newDataValidation()
    .requireValueInRange(dropdown_values, true)
    .setAllowInvalid(true)
    .build()
  range.setDataValidation(dropdown);
}