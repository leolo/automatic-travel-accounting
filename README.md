# EasyAccounting
### A small tool to keep track of your groups spendings and account the spending per person. Thought for groups of e.g. friends/family where a random person who has money with them pay. You just keep track of your spendings with EasyAccounting and after you are finished with your event (holiday, ...) just account your expenses per person with a button press. The result will be a list who has to pay whom which amount.

## Guide
After adding EasyAccounting to your sheet, reload the page.
Afterwards should "EasyAccounting" be available in your Add-ons tab.

Here should be the following functions available:
- "Setup": Sets the EasyAccounting table structure up.
- "Create Example": Fills the tables with an example project to give you a better idea how to use EasyAccounting.
- "Billing": Calculates what every person has to pay whom to settle the bill. The result of this will be available in the new created sheet "Compensation Payments".

About the different sheets:
- "Billing": Where you enter your expenses. In the column "Name" enter the name of the person which payed. In the "Accounting Area" sheet enter the related accounting area. The other fields can be omited and the field "Final Billing" is a system field (used from the accounting algorithm), which must not be changed.
- "Persons": Just enter all participating people in the "Name" column. In the "Expenses" column you are able to see the expenses per person.
- "Accounting Areas": Just enter all accounting areas. This are thought to be an abstraction of who will pay which expenses. In the "Expenses" column you again can see the expenses per accounting area. 
- "Persons Accounting Areas": A table where persons get related to accounting areas, where these persons participated and will pay there share.

### Example:
Based on the exempel you can generate under "EasyAccounting">"Create Example".

*You are on skiing holidays with friends.* 

Enter the name of you and your friends in the table persons. Some expenses relate to all like the spendings on food. But only some of you are going skiing. And only some others drink beer.

To split the expenses correctly create in the table "Abrechnungskreise" a accounting area for general expenses like "general". A nother one for shared skiing expenses named "skiing". And the last one for your fellow beer drinkers like "beer".

Now set up the "Persons Accounting Areas". This table is thought to connect the persons with the accounting areas. So you need to add rows for all your people with the accounting area "general" and then all your skiing persons to the accounting area "skiing". And obviously also all beer drinking persons to the "beer" accounting area. 

*You spent 100€ on groceries for your whole group.*

Enter in the "Billing" table your name, what you paid for "groceries" and if you want a date. And don't forget the 100€ in the amount column you spent. Because this expanse relates to all of your group choose the accounting area "general".

Thats it!

After you once understand it you can account easy smaller events (I hope so atleast :). 

After your event is done just click "EasyAccounting">"Billing".
And you will get a list of who needs to pay whom, for a fair share of the bill, in the sheet "Componsation Payments".

If you forgot an expanse just add it and do "EasyAccounting">"Billing" again.

## Development

You want to add some functionalities?
Of course you can. 

Just clone this repository and use clasp https://developers.google.com/apps-script/guides/clasp to program locally. Else you can copy this project in to Google Apps Script that should also work.

